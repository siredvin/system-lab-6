package com.darkempire.lab.systemanalisys;

import com.darkempire.anji.util.Util;
import com.darkempire.math.struct.vector.DoubleVector;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.*;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by siredvin on 27.03.15.
 *
 * @author siredvin
 */
public class SquareChartBuilder {
    public static final String[] styles = new String[]{
        "-fx-stroke:red;-fx-stroke-width:3;",
            "-fx-stroke:green;-fx-stroke-width:3;",
            "-fx-stroke:brown;-fx-stroke-width:3;",
            "-fx-stroke:blue;-fx-stroke-width:3;"
    };

    public static final String[] ellipseStyles = new String[]{
            "-fx-fill:red",
            "-fx-fill:green",
            "-fx-fill:brown",
            "-fx-fill:blue"
    };

    public static final int size = 300;
    public static final int small_size = 30;
    public static final int space = 10;
    public static void createSquareChart(Pane pane,DoubleVector v1,DoubleVector v2,DoubleVector v3,DoubleVector v4){
        Line yAxis = new Line(size,0,size,size*2),
        xAxis = new Line(0,size,size*2,size);
        List<Path> squaresPath = new ArrayList<>();
        String pathStyle = "-fx-stroke:gray; -fx-stroke-dash-array: 5,5; -fx-stroke-width: 0.75";
        String axisStyle = "-fx-stroke:gray; -fx-stroke-width: 3";
        yAxis.setStyle(axisStyle);
        xAxis.setStyle(axisStyle);
        for (int i=1;i<11;i++){
            Path tempPath = new Path(new MoveTo(size,size-small_size*i),new LineTo(size-small_size*i,size),new LineTo(size,size+small_size*i),new LineTo(size+small_size*i,size),new ClosePath());
            tempPath.setStyle(pathStyle);
            squaresPath.add(tempPath);
        }
        Text qualutyText = new Text(0,size-space/2,"Якість");
        Text corrosionText = new Text(size+space/2,0,"Рівень коррозії");
        Text automationText = new Text(size+space/2,2*size,"Рівень автоматизації");
        Text priceText = new Text(2*size,size-space/2,"Ціна");
        List<Path> seriesPath = new ArrayList<>();
        for (int i=0;i<4;i++){
            Path tempPath = new Path(new MoveTo(size-small_size*10*v1.get(i),size),new LineTo(size,size+small_size*10*v2.get(i)),new LineTo(size+small_size*10*v4.get(i),size),new LineTo(size,size-small_size*10*v3.get(i)),new ClosePath());
            tempPath.setStyle(styles[i]);
            seriesPath.add(tempPath);
        }
        squaresPath.forEach(p-> {
            p.setTranslateY(space);
            p.setTranslateX(space);
        });
        seriesPath.forEach(p->{
            p.setTranslateY(space);
            p.setTranslateX(space);
        });
        Util.<Shape>forEach(p->{
            p.setTranslateX(space);
            p.setTranslateY(space);
        },yAxis,xAxis,qualutyText,corrosionText,automationText,priceText);
        pane.getChildren().addAll(qualutyText,corrosionText,automationText,priceText);
        pane.getChildren().addAll(squaresPath);
        pane.getChildren().addAll(seriesPath);
        pane.getChildren().addAll(yAxis, xAxis);
    }
}
