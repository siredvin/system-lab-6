package com.darkempire.lab.systemanalisys;

import com.darkempire.anji.log.Log;
import com.darkempire.anjifx.beans.property.AnjiDoubleProperty;
import com.darkempire.anjifx.beans.property.AnjiStringProperty;
import javafx.geometry.Insets;
import javafx.scene.control.TableCell;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by siredvin on 23.03.15.
 *
 * @author siredvin
 */
public class GradeTableRow {
    public static final String[] millerText = new String[]{
            "Наздвичайно не ефективно", "Дуже не ефективно",
            "Не ефективно","Середня ефективність",
            "Ефективно", "Дуже ефективно", "Надзвичайно ефективно"
    };

    private AnjiStringProperty name;
    private AnjiDoubleProperty quality;
    private AnjiDoubleProperty corrosion;
    private AnjiDoubleProperty automation;
    private AnjiDoubleProperty price;
    private AnjiDoubleProperty weight;


    public GradeTableRow(String name, double quality,double corrosion,double automation,double price,double weight){
        this.name = new AnjiStringProperty("name",name);
        this.quality = new AnjiDoubleProperty("quality",quality);
        this.corrosion = new AnjiDoubleProperty("corrosion",corrosion);
        this.automation = new AnjiDoubleProperty("automation",automation);
        this.price = new AnjiDoubleProperty("price",price);
        this.weight = new AnjiDoubleProperty("weight",weight);
    }

    public AnjiStringProperty nameProperty(){
        return name;
    }

    public AnjiDoubleProperty qualityProperty(){
        return quality;
    }

    public AnjiDoubleProperty corrosionProperty(){
        return corrosion;
    }

    public AnjiDoubleProperty automationProperty(){
        return automation;
    }

    public AnjiDoubleProperty priceProperty(){
        return price;
    }

    public AnjiDoubleProperty weightProperty(){
        return weight;
    }

    public static class GradeTableCell extends TableCell<GradeTableRow,Double>{
        @Override
        protected void updateItem(Double item, boolean empty) {
            super.updateItem(item, empty);
            if (item!=null){
                int index = MainController.millerLevels.find(item);
                setText(millerText[index]);
                int currentChannel = index - 3;
                if (currentChannel>1){
                    Color color = Color.GREEN;
                    Color.color(color.getRed(),color.getGreen()-currentChannel/4.0,color.getBlue());
                    setTextFill(color);
                } else {
                    if (currentChannel<0){
                        Color color = Color.RED;
                        Color.color(color.getRed()+currentChannel/4.0,color.getGreen(),color.getBlue());
                        setTextFill(color);
                    }
                }
            }
        }
    }
}
