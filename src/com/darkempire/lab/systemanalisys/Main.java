package com.darkempire.lab.systemanalisys;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by siredvin on 21.03.15.
 *
 * @author siredvin
 */
public class Main extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("res/main.fxml"));
        Scene scene = new Scene(loader.load());
        primaryStage.setScene(scene);
        primaryStage.setHeight(700);
        primaryStage.setWidth(800);
        primaryStage.setTitle("Лабораторна робота №6");
        MainController controller = loader.getController();
        primaryStage.show();
    }

    public static void main(String[] args) {
        Main.launch(args);
    }
}
