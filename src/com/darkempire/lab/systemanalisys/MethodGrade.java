package com.darkempire.lab.systemanalisys;

import com.darkempire.anji.log.Log;
import com.darkempire.math.operator.vector.DoubleVectorMathOperator;
import com.darkempire.math.struct.function.FDoublesDouble;
import com.darkempire.math.struct.matrix.DoubleArrayMatrix;
import com.darkempire.math.struct.matrix.DoubleMatrix;
import com.darkempire.math.struct.vector.DoubleArrayVector;
import com.darkempire.math.struct.vector.DoubleVector;
import com.darkempire.math.struct.vector.FixedVector;
import com.darkempire.math.struct.vector.Vector;
import com.sun.org.apache.xml.internal.utils.IntVector;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.function.IntConsumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.darkempire.lab.systemanalisys.MainController.*;

/**
 * Created by siredvin on 21.03.15.
 *
 * @author siredvin
 */
public class MethodGrade {
    public String methodName;
    public Grade qualityGrade;
    public Grade corrosionLevelGrade;
    public Grade automationLevelGrade;
    public Grade priceGrade;

    private MethodGrade( Grade qualityGrade, Grade corrosionLevelGrade,Grade automationLevelGrade, Grade priceGrade,String name) {
        this.automationLevelGrade = automationLevelGrade;
        this.corrosionLevelGrade = corrosionLevelGrade;
        this.priceGrade = priceGrade;
        this.qualityGrade = qualityGrade;
        this.methodName=name;
    }

    public void make(){
        qualityGrade.make();
        corrosionLevelGrade.make();
        automationLevelGrade.make();
        priceGrade.make();
    }

    public static MethodGrade loadMethod(File qualityFile,File corrosionLevelFile,File automationLevelFile,File praceFile,String name) throws FileNotFoundException, ParseException {
        Grade g1 = Grade.loadGrade(qualityFile,"Якість готового виробу"),
                g2 = Grade.loadGrade(corrosionLevelFile,"Рівень корозії у процесі експлуатації"),
                g3 = Grade.loadGrade(automationLevelFile,"Рівень автоматизації"),
                g4 = Grade.loadGrade(praceFile,"Фінансові витрати на впровадження технології");
        return new MethodGrade(g1,g2,g3,g4,name);
    }

    public static class Grade{
        public String gradeName;
        public DoubleMatrix muMatrix;
        public DoubleMatrix nuMatrix;
        public DoubleMatrix dPositiveMatrix;
        public DoubleMatrix dNegativeMatrix;
        public DoubleVector middleNegativeVector;
        public DoubleVector middlePositiveVector;
        public DoubleVector qEqualNegativeVector;
        public DoubleVector qEqualPositiveVector;
        public double kPositive;
        public double kNegative;
        public double[] positiveFactors;
        public double[] negativeFactors;
        public DoubleVector qFindPositiveVector,qFindNegativeVector;
        public DoubleVector omegaVector;
        public DoubleMatrix expertDistanceMatrix;
        public DoubleVector medianVector;
        public int medianExpert;
        public DoubleVector confidenceIntervalVector;
        public int concertedGradeLevel;
        public double concertedGrade;
        public Set<Integer> confidenceExpertSet;

        private Grade(DoubleMatrix muMatrix, DoubleMatrix nuMatrix,String name) {
            this.muMatrix = muMatrix;
            this.nuMatrix = nuMatrix;
            dPositiveMatrix = dNegativeMatrix = null;
            this.gradeName=name;
        }

        private void calculateIntervals(){
            dPositiveMatrix = DoubleArrayMatrix.fixed(16,7);
            dNegativeMatrix = DoubleArrayMatrix.fixed(16,7);
            for (int rowIndex=0;rowIndex<16;rowIndex++){
                for (int columnIndex=0;columnIndex<7;columnIndex++){
                    double mu = muMatrix.get(rowIndex,columnIndex);
                    double nu = nuMatrix.get(rowIndex, columnIndex);
                    double dNegative = Math.max(0, Math.abs(mu-mu*(1-nu)*K));
                    double dPositive = Math.min(1, mu+mu*(1-nu)*K);
                    dPositiveMatrix.set(rowIndex,columnIndex,dPositive);
                    dNegativeMatrix.set(rowIndex,columnIndex,dNegative);
                }
            }
        }

        private void calculateMathMiddle(){
            calculateIntervals();
            middleNegativeVector = DoubleArrayVector.fixed(7);
            middlePositiveVector = DoubleArrayVector.fixed(7);
            middleNegativeVector.fill(i -> DoubleVectorMathOperator.sum(dNegativeMatrix.column(i)) / 16);
            middlePositiveVector.fill(i -> DoubleVectorMathOperator.sum(dPositiveMatrix.column(i)) / 16);
        }

        private void calculateIntervalIntegratedGrade(){
            calculateMathMiddle();
            //TODO:тут у прикладі було трошки незрозуміло
            qEqualNegativeVector = DoubleArrayVector.fixed(7);
            qEqualPositiveVector = DoubleArrayVector.fixed(7);
            for (int scaleIndex =0;scaleIndex<7;scaleIndex++){
                double middleNegative = middleNegativeVector.get(scaleIndex);
                double middlePositive = middlePositiveVector.get(scaleIndex);
                double qNegative = dNegativeMatrix.get(0,scaleIndex);
                double qPositive = dPositiveMatrix.get(0,scaleIndex);
                for (int rowIndex=1;rowIndex<16;rowIndex++){
                    double currentNegative = dNegativeMatrix.get(rowIndex,scaleIndex);
                    double currentPositive = dPositiveMatrix.get(rowIndex,scaleIndex);
                    if (Math.abs(currentNegative-middleNegative)<Math.abs(qNegative-middleNegative)){
                        qNegative = currentNegative;
                    }
                    if (Math.abs(currentPositive-middlePositive)<Math.abs(qPositive-middlePositive)){
                        qPositive = currentPositive;
                    }
                }
                qEqualNegativeVector.set(scaleIndex,qNegative);
                qEqualPositiveVector.set(scaleIndex,qPositive);
            }
        }

        private void calculateNormalizationFactor(){
            calculateIntervalIntegratedGrade();
            kPositive = kNegative = 0;
            for (int i=0;i<7;i++){
                kPositive+= (X_LEVEL[i+1]-X_LEVEL[i])*qEqualPositiveVector.get(i)/2;
                kNegative+= (X_LEVEL[i+1]-X_LEVEL[i])*qEqualNegativeVector.get(i)/2;
            }
            kPositive=1/kPositive;
            kNegative = 1/kNegative;
        }

        //TODO:з’ясвати про те, що саме потрібно мінімізувати
        private void calculateGaussianFactors(){
            calculateNormalizationFactor();
            kPositive = kNegative = 1;
            FDoublesDouble positiveFunction = (xs)->{
                double d = xs[1];
                double m = xs[0];
                double sum = 0;
                for (int s=0;s<7;s++){
                    sum += Math.abs(kPositive*Math.sqrt(1/(2*Math.PI*d))*Math.exp(-Math.pow(X_LEVEL[s+1]-m,2)/(2*d))- qEqualPositiveVector.get(s));
                }
                return sum;
            };
            FDoublesDouble negativeFunction = (xs)->{
                double d = xs[1];
                double m = xs[0];
                double sum = 0;
                for (int s=0;s<7;s++){
                    sum += Math.abs(kNegative*Math.sqrt(1/(2*Math.PI*d))*Math.exp(-Math.pow(X_LEVEL[s+1]-m,2)/(2*d))- qEqualNegativeVector.get(s));
                }
                return sum;
            };
            positiveFactors = LabUtil.minimizationTwoParams(positiveFunction,0.005,1,0.005);
            negativeFactors = LabUtil.minimizationTwoParams(negativeFunction,0.005,1,0.005);
            qFindNegativeVector = DoubleArrayVector.fixed(7);
            qFindPositiveVector = DoubleArrayVector.fixed(7);
            qFindNegativeVector.fill(s->kNegative*Math.sqrt(1/(2*Math.PI*negativeFactors[1]))*Math.exp(-Math.pow(X_LEVEL[s+1]-negativeFactors[0],2)/(2*negativeFactors[1])));
            qFindPositiveVector.fill(s->kPositive*Math.sqrt(1/(2*Math.PI*positiveFactors[1]))*Math.exp(-Math.pow(X_LEVEL[s+1]-positiveFactors[0],2)/(2*positiveFactors[1])));
        }

        public void calculateQuality(){
            calculateGaussianFactors();
            omegaVector = DoubleArrayVector.fixed(16);
            for (int index=0;index<16;index++) {
                double rho = 0;
                for (int scaleIndex = 0; scaleIndex < 7; scaleIndex++) {
                    rho+=Math.max(Math.abs(qFindNegativeVector.get(scaleIndex)-dNegativeMatrix.get(index,scaleIndex)),
                            Math.abs(qFindPositiveVector.get(scaleIndex)-dPositiveMatrix.get(index,scaleIndex)));
                }
                rho/=7;
                omegaVector.set(index,(1-rho)*EXPERT_GRADE[index]);
            }
        }

        public void calculateMedian(){
            calculateQuality();
            expertDistanceMatrix = DoubleArrayMatrix.fixed(16,16);
            expertDistanceMatrix.fill((rowIndex,columnIndex)->{
                double sum = 0;
                for (int s=0;s<7;s++){
                    sum+=Math.max(Math.abs(dPositiveMatrix.get(rowIndex,s)-dPositiveMatrix.get(columnIndex,s)),
                            Math.abs(dNegativeMatrix.get(rowIndex,s)-dNegativeMatrix.get(columnIndex,s)));
                }
                return sum/7;
            });
            medianVector = DoubleArrayVector.fixed(16);
            medianVector.fill(i-> DoubleVectorMathOperator.sum(expertDistanceMatrix.row(i)));
            medianExpert = 0;
            double medianValue = medianVector.get(medianExpert);
            for (int i=0;i<16;i++){
                double current = medianVector.get(i);
                if (current<medianValue){
                    medianValue = current;
                    medianExpert = i;
                }
            }
        }

        public void calculateConfidenceInterval(){
            calculateMedian();
            confidenceIntervalVector = DoubleArrayVector.fixed(16);
            for (int index=0;index<16;index++){
                double sum = 0;
                for (int s =0;s<7;s++){
                    sum+=Math.max(Math.abs(dPositiveMatrix.get(index,s)-dPositiveMatrix.get(medianExpert,s)),
                            Math.abs(dNegativeMatrix.get(index,s)- dNegativeMatrix.get(medianExpert,s)));
                }
                sum/=7;
                confidenceIntervalVector.set(index,sum*(2-omegaVector.get(index)));
            }
            confidenceExpertSet = new HashSet<>();
            IntStream.of(EXPERT_NUMBERS).filter(i -> confidenceIntervalVector.get(i) < MainController.R_T1).forEach(confidenceExpertSet::add);
        }

        public void analyseGrades(){
            calculateConfidenceInterval();
            List<Integer> integerList = new ArrayList<>();
            integerList.add(0);
            double max = muMatrix.get(medianExpert,0);
            for (int s=0;s<7;s++){
                double value = muMatrix.get(medianExpert,s);
                if (max<value){
                    integerList.clear();
                    integerList.add(s);
                    max = value;
                }
                if (max == value){
                    integerList.add(s);
                }
            }
            if (integerList.size()>1){
                int trueValue = integerList.get(0);
                double value = Math.abs(dNegativeMatrix.get(medianExpert,0) - dPositiveMatrix.get(medianExpert,0));
                for (Integer v: integerList){
                    double tValue = Math.abs(dNegativeMatrix.get(medianExpert,v) - dPositiveMatrix.get(medianExpert,v));
                    if (tValue<value){
                        value = tValue;
                        trueValue = v;
                    }
                }
                integerList.clear();
                integerList.add(trueValue);
            }
            concertedGradeLevel = integerList.get(0);
            concertedGrade = (dNegativeMatrix.get(medianExpert,concertedGradeLevel) + dPositiveMatrix.get(medianExpert,concertedGradeLevel))/2;
        }


        public void make(){
            analyseGrades();
        }

        public static Grade loadGrade(File gradeFile,String name) throws FileNotFoundException, ParseException {
            Scanner in = new Scanner(new FileInputStream(gradeFile));
            DoubleMatrix nuMatrix = DoubleArrayMatrix.fixed(16,7),
            muMatrix = DoubleArrayMatrix.fixed(16,7);
            Locale temp = Locale.getDefault();
            NumberFormat format = NumberFormat.getNumberInstance(Locale.forLanguageTag("ru"));
            for (int i=0;i<16;i++){
                String[] line = in.nextLine().split(";");
                muMatrix.fillRow(i,format.parse(line[2]).doubleValue(),
                        format.parse(line[4]).doubleValue(),
                        format.parse(line[6]).doubleValue(),
                        format.parse(line[8]).doubleValue(),
                        format.parse(line[10]).doubleValue(),
                        format.parse(line[12]).doubleValue(),
                        format.parse(line[14]).doubleValue());
                nuMatrix.fillRow(i,format.parse(line[3]).doubleValue(),
                        format.parse(line[5]).doubleValue(),
                        format.parse(line[7]).doubleValue(),
                        format.parse(line[9]).doubleValue(),
                        format.parse(line[11]).doubleValue(),
                        format.parse(line[13]).doubleValue(),
                        format.parse(line[15]).doubleValue());
            }
            Locale.setDefault(temp);
            return new Grade(muMatrix,nuMatrix,name);
        }
    }
}
