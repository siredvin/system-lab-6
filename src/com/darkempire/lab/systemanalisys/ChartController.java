package com.darkempire.lab.systemanalisys;

import com.darkempire.anji.log.Log;
import com.darkempire.anjifx.scene.chart.AnjiLineChart;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;


import static com.darkempire.lab.systemanalisys.LabUtil.createSeries;
import static com.darkempire.lab.systemanalisys.MainController.EXPERT_NUMBERS;
import static com.darkempire.lab.systemanalisys.MainController.millerLevels;
/**
 * Created by siredvin on 24.03.15.
 *
 * @author siredvin
 */
public class ChartController {
    @FXML
    private AnjiLineChart<Double,Double> mainChart;
    @FXML
    private CheckBox integratedGrade;
    @FXML
    private CheckBox verifiedGrade;
    @FXML
    private CheckBox lowerQualityExpert;

    @FXML
    private CheckBox intervalDestiny;

    @FXML
    private CheckBox medianExpertBox;

    @FXML
    private CheckBox upperQualityExpert;

    private MethodGrade.Grade grade;
    private int[] confidenceExpertNumbers;
    private int[] unConfidenceExpertNumber;

    @FXML
    void initialize() {
    }

    public void init(MethodGrade.Grade grade){
        this.grade = grade;
        confidenceExpertNumbers = new int[grade.confidenceExpertSet.size()];
        int counter = 0;
        for (Integer i:grade.confidenceExpertSet){
            confidenceExpertNumbers[counter] = i;
            counter++;
        }
        unConfidenceExpertNumber = new int[16-confidenceExpertNumbers.length];
        counter = 0;
        for (int i=0;i<16;i++){
            if (!grade.confidenceExpertSet.contains(i)){
                unConfidenceExpertNumber[counter] = i;
                counter++;
            }
        }
        for (int i=0;i<16;i++){
            mainChart.getData().add(createSeries("Експерт №"+(i+1),millerLevels,grade.muMatrix.row(i)));
        }

        Bindings.bindBidirectional(medianExpertBox.selectedProperty(), mainChart.lineStrokeVisiblite(grade.medianExpert));
        int min = grade.omegaVector.find(grade.omegaVector.min());
        int max = grade.omegaVector.find(grade.omegaVector.max());
        Bindings.bindBidirectional(lowerQualityExpert.selectedProperty(),mainChart.lineStrokeVisiblite(min));
        Bindings.bindBidirectional(upperQualityExpert.selectedProperty(), mainChart.lineStrokeVisiblite(max));

        mainChart.getData().addAll(createSeries("Оцінка-", millerLevels, grade.qEqualNegativeVector),
                createSeries("Оцінка+", millerLevels, grade.qEqualPositiveVector),
                createSeries("Медіана-", millerLevels, grade.middleNegativeVector),
                createSeries("Медіана+", millerLevels, grade.middlePositiveVector),
                createSeries("Щільність-", millerLevels, grade.qFindNegativeVector),
                createSeries("Щільність+", millerLevels, grade.qFindPositiveVector));

        intervalDestiny.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                mainChart.showSeries(20, 21);
            } else {
                mainChart.hideSeries(20, 21);
            }
        });

        verifiedGrade.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue){
                mainChart.showSeries(18,19);
            } else {
                mainChart.hideSeries(18,19);
            }
        });

        integratedGrade.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue){
                mainChart.showSeries(16,17);
            } else {
                mainChart.hideSeries(16,17);
            }
        });
    }

    @FXML
    private void showUntrustExperts(ActionEvent event) {
        hideSpecialSeries();
        mainChart.hideSeries(confidenceExpertNumbers);
        mainChart.showSeries(unConfidenceExpertNumber);
    }
    @FXML
    private void showTrustExperts(ActionEvent event) {
        hideSpecialSeries();
        mainChart.showSeries(confidenceExpertNumbers);
        mainChart.hideSeries(unConfidenceExpertNumber);
    }
    @FXML
    private void showAllExperts(ActionEvent event) {
        hideSpecialSeries();
        mainChart.showSeries(EXPERT_NUMBERS);
    }
    @FXML
    private void hideAllExperts(ActionEvent event) {
        hideSpecialSeries();
        mainChart.hideSeries(EXPERT_NUMBERS);
    }

    private void hideSpecialSeries(){
        integratedGrade.setSelected(false);
        intervalDestiny.setSelected(false);
        verifiedGrade.setSelected(false);
    }
}
