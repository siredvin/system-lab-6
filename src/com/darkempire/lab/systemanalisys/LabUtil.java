package com.darkempire.lab.systemanalisys;

import com.darkempire.anji.log.Log;
import com.darkempire.math.struct.function.FDoublesDouble;
import com.darkempire.math.struct.matrix.DoubleArrayMatrix;
import com.darkempire.math.struct.matrix.DoubleMatrix;
import com.darkempire.math.struct.vector.DoubleVector;
import javafx.collections.FXCollections;
import javafx.scene.chart.XYChart;

/**
 * Created by siredvin on 21.03.15.
 *
 * @author siredvin
 */
public final class LabUtil {
    private LabUtil(){}

    public static double[] minimizationTwoParams(FDoublesDouble function,double start,double end,double step){
        int stepCount = (int) ((end-start)/step);
        int count = stepCount;
        if (step*stepCount+start!=end) {
            count++;
        }
        DoubleMatrix valuesGrid = DoubleArrayMatrix.fixed(count,count);
        valuesGrid.fill((rowIndex,columnIndex)->function.calc(step*rowIndex+start,step*columnIndex+start));
        double min = valuesGrid.get(0,0);
        int minRowIndex = 0;
        int minColumnIndex =0;
        for (int rowIndex =0;rowIndex<count;rowIndex++){
            for (int columnIndex=0;columnIndex<count;columnIndex++){
                double v = valuesGrid.get(rowIndex,columnIndex);
                if (v<min){
                    min =v ;
                    minRowIndex = rowIndex;
                    minColumnIndex = columnIndex;
                }
            }
        }
        return new double[]{minRowIndex*step + start,minColumnIndex*step + start};
    }

    public static XYChart.Series<Double,Double> createSeries(String name,DoubleVector x,DoubleVector y){
        int size = x.getSize();
        if (size!=y.getSize()){
            return null;
        }
        XYChart.Series<Double,Double> series = new XYChart.Series<>(name, FXCollections.observableArrayList());
        for (int i=0;i<size;i++){
            series.getData().add(new XYChart.Data<>(x.get(i), y.get(i)));
        }
        return series;
    }

    public static XYChart.Series<Double,Double> createSeries(DoubleVector x,DoubleVector y){
        int size = x.getSize();
        if (size!=y.getSize()){
            return null;
        }
        XYChart.Series<Double,Double> series = new XYChart.Series<>();
        for (int i=0;i<size;i++){
            series.getData().add(new XYChart.Data<>(x.get(i), y.get(i)));
        }
        return series;
    }
}
