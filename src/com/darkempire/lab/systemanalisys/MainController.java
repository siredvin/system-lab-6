package com.darkempire.lab.systemanalisys;

import com.darkempire.anji.log.Log;
import com.darkempire.anji.util.Util;
import com.darkempire.anjifx.scene.chart.AnjiLineChart;
import com.darkempire.math.MathMachine;
import com.darkempire.math.struct.matrix.DoubleArrayMatrix;
import com.darkempire.math.struct.matrix.DoubleMatrix;
import com.darkempire.math.struct.vector.DoubleArrayVector;
import com.darkempire.math.struct.vector.DoubleVector;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.darkempire.math.struct.vector.DoubleVector.wrapDefault;

/**
 * Created by siredvin on 21.03.15.
 *vk
 * @author siredvin
 */
public class MainController {
    @FXML
    private Pane chartPane;
    @FXML
    private VBox chartLegend;
    private List<MethodGrade> methodGradeList;
    public static final double[] W_ARRAY = new double[]{0.3,0.4,0.2,0.5};
    public static final double K = 0.8;
    public static final double S_AST = 0.65;
    public static final double R_T1 = 0.3;
    public static final double[] EXPERT_GRADE = new double[]{0.70,0.95,0.93,0.89,0.77,0.79,0.91, 0.33,0.93,0.71,0.92,0.91,0.43,0.83,0.95,0.73};
    public static final double[] X_LEVEL = new double[]{0,0.07, 0.21, 0.36, 0.5, 0.64, 0.79, 0.93,1};
    public static final int[] EXPERT_NUMBERS = new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    public static final DoubleVector millerLevels = DoubleVector.wrapDefault(0.07, 0.21, 0.36, 0.5, 0.64, 0.79, 0.93);

    private DoubleMatrix gradeMatrix;
    private DoubleVector wVector;

    @FXML
    private TableView<GradeTableRow> gradesTable;
    @FXML
    private TabPane mainTabPane;
    @FXML
    private TableColumn<GradeTableRow,String> nameColumn;
    @FXML
    private TableColumn<GradeTableRow,Double> qualityColumn;
    @FXML
    private TableColumn<GradeTableRow,Double> corrosionColumn;
    @FXML
    private TableColumn<GradeTableRow,Double> automationColumn;
    @FXML
    private TableColumn<GradeTableRow,Double> priceColumn;
    @FXML
    private TableColumn<GradeTableRow,Double> weightColumn;


    private double currentChartSize;
    @FXML
    void initialize() {
        gradeMatrix = DoubleArrayMatrix.fixed(4,4);
        wVector = DoubleArrayVector.fixed(4);
        methodGradeList = new ArrayList<>();
        try {
            methodGradeList.add(MethodGrade.loadMethod(new File("./Data/Data1.1"),
                    new File("./Data/Data1.2"),
                    new File("./Data/Data1.3"),
                    new File("./Data/Data1.4"),
                    "Просочення армуючих волокон матричним матеріалом"));
            methodGradeList.add(MethodGrade.loadMethod(new File("./Data/Data2.1"),
                    new File("./Data/Data2.2"),
                    new File("./Data/Data2.3"),
                    new File("./Data/Data2.4"),
                    "Пакетне дифузійне зварювання моношарових стрічок компонентів"));
            methodGradeList.add(MethodGrade.loadMethod(new File("./Data/Data3.1"),
                    new File("./Data/Data3.2"),
                    new File("./Data/Data3.3"),
                    new File("./Data/Data3.4"),
                    "Холодне пресування компонентів з наступним спіканням"));
            methodGradeList.add(MethodGrade.loadMethod(new File("./Data/Data4.1"),
                    new File("./Data/Data4.2"),
                    new File("./Data/Data4.3"),
                    new File("./Data/Data4.4"),
                    "Виготовлення деталей з використанням препрегів"));
        } catch (FileNotFoundException | ParseException e) {
            Log.error(Log.logIndex,e);
        }
        qualityColumn.setCellValueFactory(new PropertyValueFactory<>("quality"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        corrosionColumn.setCellValueFactory(new PropertyValueFactory<>("corrosion"));
        automationColumn.setCellValueFactory(new PropertyValueFactory<>("automation"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));
        weightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        qualityColumn.setCellFactory(param -> new GradeTableRow.GradeTableCell());
        corrosionColumn.setCellFactory(t-> new GradeTableRow.GradeTableCell());
        automationColumn.setCellFactory(t->new GradeTableRow.GradeTableCell());
        priceColumn.setCellFactory(t->new GradeTableRow.GradeTableCell());
    }



    @FXML
    void makeTask(ActionEvent event) {
        long start = System.currentTimeMillis();
        methodGradeList.forEach(MethodGrade::make);
        long end = System.currentTimeMillis();
        Log.log(Log.logIndex, "Підрахунки завершилися за", end - start, "мілісекунд");
        gradeMatrix.fillColumn(0,(rowIndex,columnIndex)->millerLevels.get(methodGradeList.get(rowIndex).qualityGrade.concertedGradeLevel));
        gradeMatrix.fillColumn(1, (rowIndex, columnIndex) -> millerLevels.get(methodGradeList.get(rowIndex).corrosionLevelGrade.concertedGradeLevel));
        gradeMatrix.fillColumn(2, (rowIndex, columnIndex) -> millerLevels.get(methodGradeList.get(rowIndex).automationLevelGrade.concertedGradeLevel));
        gradeMatrix.fillColumn(3, (rowIndex, columnIndex) -> millerLevels.get(methodGradeList.get(rowIndex).priceGrade.concertedGradeLevel));
        DoubleVector tempVector = DoubleVector.wrapDefault(W_ARRAY);
        wVector.fill(i -> tempVector.scalar(gradeMatrix.row(i)) / 4);
        methodGradeList.forEach(m -> {
            GradeTableRow gradeTableRow = new GradeTableRow(m.methodName, millerLevels.get(m.qualityGrade.concertedGradeLevel)
                    , millerLevels.get(m.corrosionLevelGrade.concertedGradeLevel),
                    millerLevels.get(m.automationLevelGrade.concertedGradeLevel),
                    millerLevels.get(m.priceGrade.concertedGradeLevel), wVector.get(methodGradeList.indexOf(m)));
            gradesTable.getItems().add(gradeTableRow);
        });
        mainTabPane.getTabs().addAll(initChart(methodGradeList.get(0)),
                initChart(methodGradeList.get(1)),
                initChart(methodGradeList.get(2)),
                initChart(methodGradeList.get(3)));
        SquareChartBuilder.createSquareChart(chartPane,
                gradeMatrix.column(0),
                gradeMatrix.column(1), gradeMatrix.column(2), gradeMatrix.column(3));

        for (int i=3;i>-1;i--){
            Label label = new Label();
            label.setText(methodGradeList.get(i).methodName);
            Ellipse graphics = new Ellipse(5,5);
            graphics.setStyle(SquareChartBuilder.ellipseStyles[i]);
            label.setGraphic(graphics);
            chartLegend.getChildren().add(0,label);
        }
    }

    public Tab initChart(MethodGrade grade){
        String name = grade.methodName;
        Tab tab = new Tab(name);
        TabPane tabPane = new TabPane();
        tabPane.setPrefHeight(Region.USE_COMPUTED_SIZE);
        tabPane.setPrefWidth(Region.USE_COMPUTED_SIZE);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        tab.setContent(tabPane);
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("res/chartTab.fxml"));
            //Якість
            Tab qualityTab = new Tab(grade.qualityGrade.gradeName);
            qualityTab.setContent(loader.load());
            ChartController qualityController = loader.getController();
            qualityController.init(grade.qualityGrade);
            //Рівень коррозії
            loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("res/chartTab.fxml"));

            Tab corrosionTab = new Tab(grade.corrosionLevelGrade.gradeName);
            corrosionTab.setContent(loader.load());
            ChartController corrosionController = loader.getController();
            corrosionController.init(grade.corrosionLevelGrade);

            //Рівень автоматизації
            loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("res/chartTab.fxml"));

            Tab automationTab = new Tab(grade.automationLevelGrade.gradeName);
            automationTab.setContent(loader.load());
            ChartController automationController = loader.getController();
            automationController.init(grade.automationLevelGrade);

            //Фінансові затрати
            loader = new FXMLLoader();
            loader.setLocation(MainController.class.getResource("res/chartTab.fxml"));

            Tab priceTab = new Tab(grade.priceGrade.gradeName);
            priceTab.setContent(loader.load());
            ChartController priceController = loader.getController();
            priceController.init(grade.priceGrade);

            tabPane.getTabs().addAll(qualityTab,corrosionTab,automationTab,priceTab);
        } catch (IOException e) {
            Log.error(Log.logIndex,e);
        }
        return tab;
    }
}
